import { MantineProvider } from "@mantine/core";
import { Route, Routes, BrowserRouter, Navigate } from "react-router-dom";
import "@mantine/core/styles.css";
import Main from "./pages/main";
function App() {
  return (
    <MantineProvider>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={<Navigate to="/apps/2/courses/1/lessons/1" />}
          />
          <Route
            path="apps/:appId/courses/:courseId/sections/:sectionId/lessons/:lessonId"
            element={<Main />}
          />
          <Route
            path="apps/:appId/courses/:courseId/lessons/:lessonId"
            element={<Main />}
          />
          <Route
            path="apps/:appId/books/:bookId/audios/:audioId"
            element={<Main />}
          />
        </Routes>
      </BrowserRouter>
    </MantineProvider>
  );
}

export default App;
