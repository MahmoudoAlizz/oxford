import styles from "../../styles.module.css";

interface Props {
  audioPath: string | undefined;
}
const audio = ({ audioPath }: Props) => {
  return (
    <audio key={audioPath} controls className={styles.audio}>
      <source src={audioPath} type="audio/mp3" />
    </audio>
  );
};

export default audio;
