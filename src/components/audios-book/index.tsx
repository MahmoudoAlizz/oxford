import {
  Box,
  Card,
  Text,
  Image,
  Grid,
  Stack,
  Title,
  Flex,
  Center,
  ScrollArea,
} from "@mantine/core";
import data from "../../../data/new-data.json";
import { Link, useParams } from "react-router-dom";
import { audioProps, booksProps } from "../../types/data.type";
import styles from "../../styles.module.css";
import Audio from "./audio";
const index = () => {
  const { bookId, audioId } = useParams();
  const book = data.Apps[0].books?.find(
    (book: booksProps) => book.id == Number(bookId)
  );
  const items = book?.audios?.map((audio: audioProps) => (
    <Grid.Col span={2} key={audio.id}>
      <Link
        to={`/apps/1/books/${bookId}/audios/${audio.id}`}
        className={styles.link}
      >
        <Card shadow="sm" component="a" pb="0">
          <Card.Section>
            <Image
              src={book.imgPath}
              w="100%"
              fit="contain"
              mah={163}
              alt="No way!"
            />
          </Card.Section>
          <Text lineClamp={1} fw={500} size="xs" ta="center" py="2">
            {audio.name}
          </Text>
        </Card>
      </Link>
    </Grid.Col>
  ));
  return (
    <Stack bg="#1b1861" align="stretch" justify="space-between">
      <ScrollArea
        h="58vh"
        type="auto"
        offsetScrollbars
        scrollbarSize={6}
        scrollHideDelay={0}
        styles={{ thumb: { background: "white" } }}
      >
        <Title py="xs" c="white" fw={"bold"} order={2} ta="center">
          Chapters
        </Title>
        <Grid columns={14} mx="xl" mt="xs">
          {items}
        </Grid>
      </ScrollArea>
      <Box bg="#050b25" pt="xs">
        <Flex mx="xl" gap={"md"} pb={"xs"} align={"center"}>
          <Center h="70%">
            <Image
              px="md"
              radius="10"
              src={book?.imgPath}
              mah="100"
              alt="No way!"
            />
          </Center>
          <Box>
            <Title
              fz={{ base: "md", sm: "xl", md: "28px", lg: "35px" }}
              c="#f1b400"
            >
              Current Playing Info
            </Title>
            <Box c="#f1b400" my="xs">
              Book:{" "}
              <Text display={"inline"} c="white">
                {book?.name}
              </Text>
            </Box>
            <Box c="#f1b400">
              Chapter:
              <Text display={"inline"} c="white">
                {" "}
                {
                  book?.audios.find(
                    (audio: audioProps) => audio.id == Number(audioId)
                  )?.name
                }
              </Text>
            </Box>
          </Box>
        </Flex>
        <Box ml="xl">
          <Audio
            audioPath={
              book?.audios.find(
                (audio: audioProps) => audio.id == Number(audioId)
              )?.path
            }
          />
        </Box>
      </Box>
    </Stack>
  );
};

export default index;
