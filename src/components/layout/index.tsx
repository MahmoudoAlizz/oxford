import { AppShell, Burger, Center } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { ReactNode } from "react";
import Sidebar from "./sidebar";
interface Props {
  children: ReactNode;
}
const index = ({ children }: Props) => {
  const [opened, { toggle }] = useDisclosure();

  return (
    <AppShell
      withBorder={false}
      header={{ height: { base: 40, sm: 0 } }}
      navbar={{
        width: { base: 350, md: 450 },
        breakpoint: "sm",
        collapsed: { mobile: !opened },
      }}
      padding="md"
    >
      <AppShell.Header bg={"#050b25"}>
        <Center h="100%" styles={{ root: { justifyContent: "end" } }}>
          <Burger
            styles={{ root: { "--burger-color": "white" } }}
            opened={opened}
            onClick={toggle}
            hiddenFrom="sm"
            size="sm"
          />
        </Center>
      </AppShell.Header>
      <AppShell.Main bg={"#050b25"}>{children}</AppShell.Main>
      <AppShell.Navbar bg={"#050b25"}>
        <Sidebar />
      </AppShell.Navbar>
    </AppShell>
  );
};

export default index;
