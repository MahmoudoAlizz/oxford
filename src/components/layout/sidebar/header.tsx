import { Box, Center, Flex, Image, Title, Text } from "@mantine/core";
import { Link, useParams } from "react-router-dom";
import data from "../../../../data/new-data.json";
import { courseProps } from "../../../types/data.type";

const header = () => {
  const { appId, courseId } = useParams();
  return (
    <Flex
      px="md"
      gap={"lg"}
      justify={"space-around"}
      py={{ base: "0px", sm: "md" }}
    >
      <Center maw={{ base: "10%", sm: "35%" }}>
        <Link to="/">
          <Image src={"/assets/logo.png"} />
        </Link>
      </Center>
      <Box mt="lg">
        <Title order={1} fz={{ xs: "20px", md: "28px" }} c={"white"}>
          Oxford University Press
        </Title>
        <Text fz={{ xs: "14px", md: "20px" }} fw="bold" c="#f1b400">
          {appId == "1"
            ? "Audio Books"
            : data.Apps[1].courses?.find(
                (i: courseProps) => i.id == Number(courseId)
              )?.name}
        </Text>
      </Box>
    </Flex>
  );
};

export default header;
