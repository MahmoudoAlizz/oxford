import { Box, ScrollArea, List } from "@mantine/core";
import { useParams } from "react-router-dom";
import data from "../../../../data/new-data.json";
import CourseContent from "../../unit-content";
import Item from "../../unit-content/lesson-item";
import Header from "./header";
import {
  booksProps,
  courseProps,
  lessonProps,
  levelsProps,
  sectionsProps,
} from "../../../types/data.type";
import AudioItem from "../../unit-content/audio-item";
const index = () => {
  let { courseId, sectionId, appId } = useParams();
  const course: any = data.Apps[1].courses?.find(
    (i: courseProps) => i.id == Number(courseId)
  );

  const renderAnimation = () => {
    if (sectionId) {
      let section = course?.sections.find(
        (i: sectionsProps) => i.id == Number(sectionId)
      );
      return section.levels.map((l: levelsProps) => (
        <CourseContent id={l.id} key={l.id} name={l.name} lessons={l.lessons} />
      ));
    } else {
      const withLevels = Object.keys(course).find((i) => i == "levels");
      return withLevels ? (
        course.levels.map((l: levelsProps) => (
          <CourseContent
            id={l.id}
            key={l.id}
            name={l.name}
            lessons={l.lessons}
          />
        ))
      ) : (
        <List
          mx="sm"
          styles={{
            item: { listStyleType: "none", textAlign: "right" },
          }}
        >
          {course.lessons.map((i: lessonProps, index: number) => (
            <Item key={index} item={i} />
          ))}
        </List>
      );
    }
  };

  const renderAudios = () => {
    return (
      <List
        mx="sm"
        styles={{
          item: { listStyleType: "none", textAlign: "right" },
        }}
      >
        {data.Apps[0].books?.map((i: booksProps, index: number) => (
          <AudioItem key={index} item={i} />
        ))}
      </List>
    );
  };
  return (
    <Box>
      <Header />
      <ScrollArea
        mt="md"
        px="xs"
        scrollbars={"y"}
        h={"75vh"}
        type="always"
        offsetScrollbars
        styles={{
          scrollbar: {
            background: "white",
            marginRight: "4px",
            padding: 0,
          },
          thumb: { background: "#f1b400", borderRadius: 0 },
          corner: { background: "none" },
        }}
      >
        {appId == "2" ? renderAnimation() : renderAudios()}
      </ScrollArea>
    </Box>
  );
};

export default index;
