import { Box, Text } from "@mantine/core";
import Video from "./video";

interface Props {
  title: string;
  path: string;
  poster: string;
}
const lesson = ({ title, path, poster }: Props) => {
  return (
    <Box>
      <Text pl="xl" c={"white"}>
        Title : {title}
      </Text>
      <Video videoPath={path} poster={poster} />
    </Box>
  );
};

export default lesson;
