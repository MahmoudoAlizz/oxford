import { Box } from "@mantine/core";
interface Props {
  videoPath: string | undefined;
  poster: string;
}
const video = ({ videoPath, poster }: Props) => {
  return (
    <Box h={"80vh"} px="xl" pt="md" key={videoPath}>
      <video
        autoPlay
        loop
        controls
        width="100%"
        style={{ maxHeight: "95%" }}
        poster={poster}
      >
        <source src={videoPath} type="video/mp4" />
      </video>
    </Box>
  );
};

export default video;
