import { Group } from "@mantine/core";
import { useParams } from "react-router-dom";
import data from "../../../data/new-data.json";
import { sectionsProps } from "../../types/data.type";
import Item from "./item";
const SectionsBar = () => {
  const { courseId, appId } = useParams();
  const renderItems = () => {
    return data.Apps[1].courses?.map((i: any) => {
      if (Object.keys(i).find((i) => i == "sections")) {
        return (
          <Item
            key={i.id}
            active={Number(courseId) == i.id && Number(appId) == 2}
            name={i.name}
            menuData={i.sections?.map((section: sectionsProps) => ({
              name: section.name,
              goTo: `/apps/2/courses/3/sections/${section.id}/lessons/1`,
            }))}
          />
        );
      }
      return (
        <Item
          key={i.id}
          active={Number(courseId) == i.id && Number(appId) == 2}
          name={i.name}
          goTo={`/apps/2/courses/${i.id}/lessons/1`}
        />
      );
    });
  };
  return (
    <Group mb="xl" mt={{ base: "0", sm: "xl" }} px="xl" justify="center" grow>
      {renderItems()}
      <Item
        key={4}
        active={Number(appId) == 1}
        name={"Audio Books"}
        goTo={`/apps/1/books/1/audios/1`}
      />
    </Group>
  );
};

export default SectionsBar;
