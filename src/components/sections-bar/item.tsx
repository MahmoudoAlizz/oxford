import { Button, Menu, Text } from "@mantine/core";
import { useHover } from "@mantine/hooks";
import { RefObject } from "react";
import { Link } from "react-router-dom";
import styles from "../../styles.module.css";
interface Props {
  name: string;
  active: boolean;
  goTo?: string | undefined;
  menuData?: { name: string; goTo: string }[] | undefined;
}
const Item = ({ name, active, goTo, menuData }: Props) => {
  const {
    ref,
    hovered,
  }: { ref: RefObject<HTMLButtonElement>; hovered: boolean } = useHover();
  const activeStyle =
    active || hovered
      ? { background: "#1b1861", color: "white" }
      : {
          background: "#f1b400",
          color: "black",
        };
  if (!menuData && goTo) {
    return (
      <Link to={goTo} className={styles.link}>
        <Button
          style={{ ...activeStyle }}
          fullWidth
          ref={ref}
          variant="default"
          fz={{ base: "md", sm: "xs", md: "sm", lg: "md" }}
          fw={"700"}
          className={styles.borderNone}
        >
          {name}
        </Button>
      </Link>
    );
  }
  return (
    <Menu trigger="hover" width={"15%"} offset={5} withArrow>
      <Menu.Target>
        <Button
          className={styles.borderNone}
          ref={ref}
          style={{ ...activeStyle }}
          variant="default"
          fz="md"
          fw={"700"}
        >
          {name}
        </Button>
      </Menu.Target>
      <Menu.Dropdown>
        {menuData?.map((i: { name: string; goTo: string }, index: number) => (
          <Menu.Item ref={ref} key={index}>
            <Link className={styles.link} to={i.goTo}>
              <Text size="md">{i.name}</Text>
            </Link>
          </Menu.Item>
        ))}
      </Menu.Dropdown>
    </Menu>
  );
};

export default Item;
