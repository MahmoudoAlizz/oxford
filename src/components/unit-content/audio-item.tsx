import { List, Text } from "@mantine/core";
import { useHover } from "@mantine/hooks";
import { RefObject } from "react";
import { Link, useParams } from "react-router-dom";
import styles from "../../styles.module.css";
import { booksProps } from "../../types/data.type";

interface Props {
  item: booksProps;
}
const item = ({ item }: Props) => {
  const { hovered, ref }: { hovered: boolean; ref: RefObject<HTMLLIElement> } =
    useHover();
  const { bookId } = useParams();
  return (
    <Link className={styles.link} to={`/apps/1/books/${item.id}/audios/1`}>
      <List.Item
        ref={ref}
        px="xs"
        pl={hovered ? "5px" : ""}
        py={"sm"}
        className={styles.customBorder}
        bg={Number(bookId) == item.id ? "#f1b400" : hovered ? "#1b1861" : ""}
        ta="left"
      >
        <Text c="white">{item.name}</Text>
      </List.Item>
    </Link>
  );
};

export default item;
