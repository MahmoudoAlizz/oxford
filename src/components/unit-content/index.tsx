import {
  Box,
  Button,
  Text,
  Collapse,
  Flex,
  Group,
  List,
  Center,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { IconChevronDown, IconChevronUp } from "@tabler/icons-react";
import { lessonProps } from "../../types/data.type";
import LessonItem from "./lesson-item";
interface Props {
  name: string;
  id: number;
  lessons?: lessonProps[];
}
const unitsContent = ({ name, lessons, id }: Props) => {
  const [opened, { toggle }] = useDisclosure(id == 1);

  const renderLessons = (
    <List
      mx="sm"
      styles={{
        item: { listStyleType: "none", textAlign: "right" },
      }}
    >
      {lessons?.map((i: lessonProps, index: number) => (
        <LessonItem key={index} item={i} />
      ))}
    </List>
  );

  return (
    <Box my="xs">
      <Group justify="center" mb={5}>
        <Button
          styles={{
            inner: { justifyContent: "start" },
            label: { width: "100%" },
          }}
          variant="subtle"
          fullWidth
          c="yellow"
          onClick={toggle}
          ta="left"
        >
          <Flex w="100%" justify={"space-between"}>
            <Text>{name}</Text>
            <Center>
              {opened ? (
                <IconChevronUp size="15px" />
              ) : (
                <IconChevronDown size="15px" />
              )}
            </Center>
          </Flex>
        </Button>
      </Group>
      <Collapse in={opened}>{lessons ? renderLessons : ""}</Collapse>
    </Box>
  );
};

export default unitsContent;
