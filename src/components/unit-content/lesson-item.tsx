import { List, Text } from "@mantine/core";
import { useHover } from "@mantine/hooks";
import { RefObject } from "react";
import { Link, useParams } from "react-router-dom";
import styles from "../../styles.module.css";
import { lessonProps } from "../../types/data.type";

interface Props {
  item: lessonProps;
}
const item = ({ item }: Props) => {
  const { hovered, ref }: { hovered: boolean; ref: RefObject<HTMLLIElement> } =
    useHover();
  const { courseId, lessonId, sectionId } = useParams();
  return (
    <Link
      className={styles.link}
      to={`/apps/2/courses/${courseId}${
        sectionId ? `/sections/${sectionId}` : ""
      }/lessons/${item.id}`}
    >
      <List.Item
        ref={ref}
        px="xs"
        pl={hovered ? "5px" : ""}
        py={"sm"}
        className={styles.customBorder}
        bg={Number(lessonId) == item.id ? "#f1b400" : hovered ? "#1b1861" : ""}
        ta="left"
      >
        <Text c="white">{item.name}</Text>
      </List.Item>
    </Link>
  );
};

export default item;
