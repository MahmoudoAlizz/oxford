import { useParams } from "react-router-dom";
import Layout from "../components/layout";
import Lesson from "../components/lesson";
import SectionsBar from "../components/sections-bar";
import data from "../../data/new-data.json";
import AudioBook from "../components/audios-book";
import {
  courseProps,
  lessonProps,
  levelsProps,
  sectionsProps,
} from "../types/data.type";

const Course = () => {
  const { appId, courseId, lessonId, sectionId } = useParams();

  const filterdLessonWithLevels = (course: any) => {
    return course.levels.flatMap((level: levelsProps) =>
      level.lessons.filter(
        (lesson: lessonProps) => lesson.id == Number(lessonId)
      )
    )[0];
  };
  const renderLesson = (l: lessonProps) => {
    return <Lesson poster={l.poster} key={l.id} title={l.name} path={l.path} />;
  };

  const renderView = () => {
    if (appId == "2") {
      const currentCourse: any = data.Apps[1].courses?.find(
        (course: courseProps) => course.id == Number(courseId)
      );
      if (sectionId) {
        const section: sectionsProps = currentCourse?.sections?.find(
          (i: sectionsProps) => i.id == Number(sectionId)
        );
        return <>{renderLesson(filterdLessonWithLevels(section))}</>;
      } else {
        return Object.keys(currentCourse).find((i: string) => i == "levels") ? (
          <>{renderLesson(filterdLessonWithLevels(currentCourse))}</>
        ) : (
          <>
            {renderLesson(
              currentCourse.lessons.filter(
                (i: lessonProps) => i.id == Number(lessonId)
              )[0]
            )}
          </>
        );
      }
    } else {
      return <AudioBook />;
    }
  };
  return (
    <Layout>
      <SectionsBar />
      {renderView()}
    </Layout>
  );
};

export default Course;
