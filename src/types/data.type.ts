// Animation App
export type lessonProps = {
  id: number;
  name: string;
  path: string;
  poster: string;
};
export type levelsProps = {
  id: number;
  name: string;
  lessons: lessonProps[];
};
export type sectionsProps = {
  id: number;
  name: string;
  levels: levelsProps[];
};
export type courseProps =
  | {
      id: number;
      name: string;
      lessons: lessonProps[];
    }
  | {
      id: number;
      name: string;
      sections: sectionsProps[];
    }
  | {
      id: number;
      name: string;
      levels: levelsProps[];
    };

//  Audios Books App
export type audioProps = {
  id: number;
  name: string;
  path: string;
};
export type booksProps = {
  id: number;
  name: string;
  imgPath: string;
  audios: audioProps[];
};

export type appsProps = {
  id: number;
  name: string;
  imgPath: string;
  courses?: courseProps[];
  books?: booksProps[];
};
